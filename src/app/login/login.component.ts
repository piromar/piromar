import { Component, OnInit } from '@angular/core';
import { LoginService } from '../service/service.index';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { User } from '../beans/admin/user/user.bean';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  valForm: FormGroup;
  remember: boolean;
  email: string;

  constructor(
    public _formBuilder: FormBuilder,
    public _loginService: LoginService,
    public _router: Router
  ) {
    this.email = localStorage.getItem('email') || '';
    if (this.email.length > 1) {
      this.remember = true;
    }
    this.valForm = _formBuilder.group({
      'email': [this.email, Validators.compose([Validators.required, Validators.email])],
      'password': [null, Validators.required],
      'remember': [this.remember]
    });
  }

  ngOnInit() {

  }

  login($ev, value: any) {
    $ev.preventDefault();
    for (let c in this.valForm.controls) {
      this.valForm.controls[c].markAsTouched();
    }
    if (!this.valForm.valid) {
      return;
    }
    let user = new User(value.email, value.password);
    this._loginService.login(user, value.remember)
      .subscribe(resp =>
          this._router.navigate(['/home'])
      );
  }

}