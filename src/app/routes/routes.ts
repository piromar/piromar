import { LayoutComponent } from '../layout/layout.component';


import { MaintenanceComponent } from './pages/maintenance/maintenance.component';
import { Error404Component } from './pages/error404/error404.component';
import { Error500Component } from './pages/error500/error500.component';

import { LoginComponent } from '../login/login.component';
import { LoginGuard } from '../service/service.index';

export const routes = [

    {
        path: '',
        canActivate: [ LoginGuard ],
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', loadChildren: './home/home.module#HomeModule' },
            { path: 'admin', loadChildren: './admin/admin.module#AdminModule'},
            { path: 'withdrawal', loadChildren: './withdrawal/withdrawal.module#WithdrawalModule'}
        ]
    },

    // Not lazy-loaded routes
    { path: 'login', component: LoginComponent },
    { path: 'maintenance', component: MaintenanceComponent },
    { path: '404', component: Error404Component },
    { path: '500', component: Error500Component },

    // Not found
    { path: '**', redirectTo: 'home' }

];
