
const Home = {
    text: 'Escritorio',
    link: '/home',
    icon: 'icon-home'
};

const Withdrawal = {
    text: 'Recogidas',
    link: '/withdrawal',
    icon: 'fa fa-truck',
    submenu: [
        {
            text: 'Carga de Recogidas',
            link: '/withdrawal/withdrawal-load'
        },
        {
            text: 'Pendientes',
            link: '/withdrawal/withdrawal-pending'
        },
        {
            text: 'Enviadas',
            link: '/withdrawal/withdrawal-send'
        }    
    ]
}

const Admin = {
    text: 'Administración',
    link: '/admin',
    icon: 'fa fa-cogs',
    submenu: [
        {
            text: 'Usuarios',
            link: '/admin/users'
        },
        {
            text: 'Productos',
            link: '/admin/products'
        },
        {
            text: 'Fabricantes',
            link: '/admin/manufacturers'
        },
        {
            text: 'Catalogos',
            link: '/admin/catalogs'
        }
    ]
};


export const menu = [
    Home, 
    Withdrawal,
    Admin
    
];


// export const menu = [
//     headingMain,
//     Home,
//     Dashboard,
//     Widgets,
//     headingComponents,
//     Elements,
//     Forms,
//     Charts,
//     Tables,
//     Maps,
//     headingMore,
//     Pages,
//     Blog,
//     Ecommerce,
//     Extras
// ];
