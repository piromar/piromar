import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';
import { MaintenanceComponent } from './maintenance/maintenance.component';
import { Error404Component } from './error404/error404.component';
import { Error500Component } from './error500/error500.component';

/* Use this routes definition in case you want to make them lazy-loaded */

@NgModule({
    imports: [
        SharedModule,
    ],
    declarations: [
        MaintenanceComponent,
        Error404Component,
        Error500Component
    ],
    exports: [
        RouterModule,
        MaintenanceComponent,
        Error404Component,
        Error500Component
    ]
})
export class PagesModule { }
