import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { WithdrawalPendingComponent } from './withdrawal-pending/withdrawal-pending.component';
import { WithdrawalSendComponent } from './withdrawal-send/withdrawal-send.component';
import { WithdrawalComponent } from './withdrawal.component';
import { WithdrawalEditComponent } from './withdrawal-edit/withdrawal-edit.component';
import { WithdrawalLoadComponent } from './withdrawal-load/withdrawal-load.component';

const routes: Routes = [
  { path: '', component: WithdrawalComponent },
  { path: 'withdrawal-load', component: WithdrawalLoadComponent },
  { path: 'withdrawal-pending', component: WithdrawalPendingComponent },
  { path: 'withdrawal-pending/:id', component: WithdrawalEditComponent },
  { path: 'withdrawal-send', component: WithdrawalSendComponent },
  { path: 'withdrawal-send/:id', component: WithdrawalEditComponent },
];

@NgModule({
  declarations: [
    WithdrawalComponent,
    WithdrawalPendingComponent,
    WithdrawalSendComponent,
    WithdrawalEditComponent,
    WithdrawalLoadComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class WithdrawalModule { }
