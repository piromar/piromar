import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { ProductsComponent } from './products/products.component';
import { ProductComponent } from './products/product.component';
import { RouterModule, Routes } from '@angular/router';
import { ManufacturersComponent } from './manufacturers/manufacturers.component';
import { ManufacturerComponent } from './manufacturers/manufacturer.component';
import { UsersComponent } from './users/users.component';
import { UserComponent } from './users/user.component';
import { CatalogsComponent } from './catalogs/catalogs.component';
import { CatalogComponent } from './catalogs/catalog.component';
import { FormsModule } from '@angular/forms';
import { NgxSelectModule } from 'ngx-select-ex'
import { SharedModule } from '../../shared/shared.module';

const routes: Routes = [
  { path: '', component: AdminComponent },
  { path: 'products', component: ProductsComponent},
  { path: 'product/:id', component: ProductComponent},
  { path: 'manufacturers', component: ManufacturersComponent},
  { path: 'manufacturer/:id', component: ManufacturerComponent},
  { path: 'users', component: UsersComponent},
  { path: 'user/:id', component: UserComponent},
  { path: 'catalogs', component: CatalogsComponent},
  { path: 'catalog/:id', component: CatalogComponent}
];

@NgModule({
  declarations: [
    AdminComponent,
    ProductsComponent, 
    ProductComponent, 
    ManufacturersComponent, 
    ManufacturerComponent, 
    UsersComponent, 
    UserComponent, 
    CatalogsComponent, 
    CatalogComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    NgxSelectModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AdminModule { }
