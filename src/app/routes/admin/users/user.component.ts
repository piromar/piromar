

import { Component, OnInit } from '@angular/core';
import { User } from '../../../beans/admin/user/user.bean';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService, ContactService, ContractService, BillingService } from '../../../service/service.index';
import { NgForm } from '@angular/forms';
import { Address } from '../../../beans/admin/user/address.bean';
import { Contact } from '../../../beans/admin/user/contact.bean';
import { Contract } from '../../../beans/admin/user/contract.bean';
import { Billing } from '../../../beans/admin/user/billing.bean';
import { AddressService } from '../../../service/admin/user/address.service';
const swal = require('sweetalert');

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styles: []
})
export class UserComponent implements OnInit {

  user: User =  new User('', '', '');
  contact: Contact = new Contact('', '', '', '', '', true);
  contract: Contract = new Contract('', '', '', '', '', '');
  billing: Billing = new Billing('', '', '', '', '', '', 0, 0, '', '', '', '');
  address: Address = new Address('', '', '', '', '', '', '', '', '', true);
  
  addresses: Address[] = [];
  editingAddress = false;
  todo: any = {};

  constructor(
    private _userService: UserService,
    private _billingService: BillingService,
    private _contactService: ContactService,
    private _contractService: ContractService,
    private _addressService: AddressService,
    private _router: Router,
    public _activatedRoute: ActivatedRoute
  ) { 

    _activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id !== 'new') 
        this.load(id);
    })

  }

  ngOnInit() {
  }

  load(id: string) {
    this._userService.retrieve(id).subscribe(resp => {
      this.user = resp;
    });

    this._billingService.findAll(id).subscribe((resp: any) => {
      if (resp.billings.length > 0) 
        this.billing = resp.billings[0];
      
    });

    this._contactService.findAll(id).subscribe((resp: any) => {
      if (resp.contacts.length > 0 ) 
        this.contact = resp.contacts[0]
    });

    this._contractService.findAll(id).subscribe((resp: any) => {
      if ( resp.contracts.length > 0 )
        this.contract = resp.contracts[0]
    });

    this._addressService.findAll(id).subscribe((resp: any) => {
      this.addresses = resp.addresses;
    });
  }

  save(f: NgForm) {
    if (f.invalid) {
      return;
    }
    if (this.user._id) {
      this._userService.update(this.user).subscribe(resp => {
        this.user = resp;
        this._router.navigate(['/admin/user', resp._id])
      })
    } else {
      this._userService.create(this.user).subscribe(resp => {
        this.user = resp;
        this._router.navigate(['/admin/user', resp._id])   
      });
    }
  }

  saveBilling(f: NgForm) {
    if (f.invalid) {
      return;
    }
    this.billing.user = this.user._id;
    if (this.billing._id) {
      this._billingService.update(this.billing).subscribe(resp => {
        this.billing = resp;
        this._router.navigate(['/admin/user', this.user._id])
      })
    } else {
      this._billingService.create(this.billing).subscribe(resp => {
        this.billing = resp;
        this._router.navigate(['/admin/user', this.user._id])   
      });
    }
  }

  saveContact(f: NgForm) {
    if (f.invalid) {
      return;
    }
    this.contact.user = this.user._id;
    if (this.contact._id) {
      this._contactService.update(this.contact).subscribe(resp => {
        this.contact = resp;
        this._router.navigate(['/admin/user', this.user._id])
      })
    } else {
      this._contactService.create(this.contact).subscribe(resp => {
        this.contact = resp;
        this._router.navigate(['/admin/user', this.user._id])   
      });
    }
  }

  saveContract(f: NgForm) {
    if (f.invalid) {
      return;
    }
    this.contract.user = this.user._id;
    if (this.contract._id) {
      this._contractService.update(this.contract).subscribe(resp => {
        this.contract = resp;
        this._router.navigate(['/admin/user', this.user._id])
      })
    } else {
      this._contractService.create(this.contract).subscribe(resp => {
        this.contract = resp;
        this._router.navigate(['/admin/user', this.user._id])   
      });
    }
  }

  saveAddress(f: NgForm) {
    if (f.invalid) {
      return;
    }
    this.address.user = this.user._id;
    if (this.todo ===  null) {
      return;
    }
    if (this.editingAddress) {
      this.todo = {};
      this.editingAddress = false;
      this._addressService.update(this.address).subscribe(resp => {
        this.address = resp;
      })
    } else {
      this._addressService.create(this.address).subscribe(resp => {
        this.address = resp;
        this.addresses.push(this.address); 
        this.address = new Address('', '', '', '', '', '', '', '', '', true);  
      });
    }

   

  }

  editAddress(index, $event) {
    $event.preventDefault();
    $event.stopPropagation();
    this.address = this.addresses[index];
    this.editingAddress = true;
  }

  removeAddress(index) {
    this.address = new Address('', '', '', '', '', '', '', '', '', true); 
    swal({
      title: '¿Esta seguro?',
      text: 'Esta a punto de borrar esta direccion' + this.titleItemAddress(this.addresses[index]),
      icon: 'warning',
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        this._addressService.delete(this.addresses[index]._id).subscribe(resp => {
          this.addresses.splice(index, 1);
        })
      }
    });
  }

  titleItemAddress(item: Address) {
    return `${item.townCode} - ${item.provinceCode} - ${item.address}`;
  }
}
