import { Component, OnInit } from '@angular/core';
import { User } from '../../../beans/admin/user/user.bean';
import { Router } from '@angular/router';
import { LoginService, UserService } from '../../../service/service.index';
const swal = require('sweetalert');

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styles: []
})
export class UsersComponent implements OnInit {

  users: User[] = [];
  from: number = 0;
  total: number = 0;
  loading: boolean = true;

  constructor(
    private _userService: UserService,
    private _loginService: LoginService,
    private _router: Router
  ){ }

  ngOnInit() {
    this.loadUsers();
  }

  loadUsers() {
    this.loading = true;
    this._userService.retrieveAll(this.from).subscribe((resp: any) => {
      this.total = resp.total;
      this.users = resp.users;
      this.loading = false;
    })
  }

  edit(user: User) {
    this._router.navigate(['/admin/user', user._id])
  }

  delete(user: User) {
    if (user._id === this._loginService.userLogin._id) {
      swal('No puede borrar usuario', 'No se puede borrar a si mismo', 'error');
      return;
    }

    swal({
      title: '¿Esta seguro?',
      text: 'Esta a punto de borrar a ' + user.name,
      icon: 'warning',
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        this._userService.delete(user._id).subscribe(resp => {
          this.from = 0;
          this.loadUsers();
        })
      }
    });

  }

  changePaginate(value: number) {
    let from  = this.from + value;

    if (from >= this.total) {
      return;
    }

    if (from < 0) {
      return;
    }
    this.from += value;
    this.loadUsers();
  }

}
