import { Component, OnInit } from '@angular/core';
import { Product } from '../../../beans/admin/product/product.bean';
import { Manufacturer } from '../../../beans/admin/product/manufacturer.bean';
import { ProductService } from '../../../service/admin/product/product.service';
import { ManufacturerService } from '../../../service/admin/product/manufacturer.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styles: []
})
export class ProductComponent implements OnInit {

  product = new Product('', '', '', new Manufacturer('', '', '', '', ''));
  manufacturers: Manufacturer[] = [];


  constructor(
    private _productService :ProductService,
    private _manufacturerService: ManufacturerService,
    private _router: Router,
    public _activatedRoute: ActivatedRoute
  ) { 

    _activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id !== 'new') 
        this.load(id);
    })

  }

  ngOnInit() {
    this._manufacturerService.retrieveAll().subscribe((resp: any) => {
      this.manufacturers = resp.manufacturers;
    });
  }

  load(id: string) {
    this._productService.retrieve(id).subscribe((resp: any) => {
      this.product = resp;
    });
  }

  save(f: NgForm) {
    if (f.invalid) {
      return;
    }
    if (this.product._id) {
      this._productService.update(this.product).subscribe(resp => {
        this.product = resp;
        this._router.navigate(['/admin/product', resp._id])
      })
    } else {
      this._productService.create(this.product).subscribe(resp => {
        this.product = resp;
        this._router.navigate(['/admin/product', resp._id])   
      });
    }
  }

}
