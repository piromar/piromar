import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from '../../../beans/admin/product/product.bean';
import { ProductService } from '../../../service/service.index';
const swal = require('sweetalert');

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styles: []
})
export class ProductsComponent implements OnInit {

  products: Product[] = [];
  from: number = 0;
  total: number = 0;
  loading: boolean = true;

  constructor(
    private _productService: ProductService,
    private _router: Router
  ) { }

  ngOnInit() {
    this.loadProducts()
  }

  loadProducts() {
    this.loading = true;
    this._productService.retrieveAll(this.from).subscribe((resp: any) => {
      this.total = resp.total;
      this.products = resp.products;
      this.loading = false;
    })
  }

  edit(product: Product) {
    this._router.navigate(['/admin/product', product._id])
  }

  delete(product: Product) {
  

    swal({
      title: '¿Esta seguro?',
      text: 'Esta a punto de borrar a ' + product.name,
      icon: 'warning',
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        this._productService.delete(product._id).subscribe(resp => {
          this.from = 0;
          this.loadProducts();
        })
      }
    });

  }

  changePaginate(value: number) {
    let from  = this.from + value;

    if (from >= this.total) {
      return;
    }

    if (from < 0) {
      return;
    }
    this.from += value;
    this.loadProducts();
  }

}
