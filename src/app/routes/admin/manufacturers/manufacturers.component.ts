import { Component, OnInit } from '@angular/core';
import { Manufacturer } from '../../../beans/admin/product/manufacturer.bean';
import { ManufacturerService } from '../../../service/admin/product/manufacturer.service';
import { Router } from '@angular/router';
const swal = require('sweetalert');

@Component({
  selector: 'app-manufacturers',
  templateUrl: './manufacturers.component.html',
  styles: []
})
export class ManufacturersComponent implements OnInit {

  manufacturers: Manufacturer[] = [];
  from: number = 0;
  total: number = 0;
  loading: boolean = true;

  constructor(
    private _manufacturerService: ManufacturerService,
    private _router: Router
  ) { }

  ngOnInit() {
    this.loadManufacturer();
  }


  loadManufacturer() {
    this.loading = true;
    this._manufacturerService.retrieveAll(this.from).subscribe((resp: any) => {
      this.total = resp.total;
      this.manufacturers = resp.manufacturers;
      this.loading = false;
    })
  }

  edit(manufacturer: Manufacturer) {
    this._router.navigate(['/admin/manufacturer', manufacturer._id])
  }

  delete(manufacturer: Manufacturer) {
    
    swal({
      title: '¿Esta seguro?',
      text: 'Esta a punto de borrar ' + manufacturer.name,
      icon: 'warning',
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        this._manufacturerService.delete(manufacturer._id).subscribe(resp => {
          this.from = 0;
          this.loadManufacturer();
        })
      }
    });

  }

  changePaginate(value: number) {
    let from  = this.from + value;

    if (from >= this.total) {
      return;
    }

    if (from < 0) {
      return;
    }
    this.from += value;
    this.loadManufacturer();
  }

}
