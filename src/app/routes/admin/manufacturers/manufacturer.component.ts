import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ManufacturerService } from '../../../service/admin/product/manufacturer.service';
import { Manufacturer } from '../../../beans/admin/product/manufacturer.bean';

@Component({
  selector: 'app-manufacturer',
  templateUrl: './manufacturer.component.html',
  styles: []
})
export class ManufacturerComponent implements OnInit {

  manufacturer = new Manufacturer('', '', '', '', '');

  constructor(
    private _manufacturerService: ManufacturerService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute
  ) { 
    _activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id !== 'new') 
        this.load(id);
    })
  }

  ngOnInit() {
  }

  load(id: string) {
    this._manufacturerService.retrieve(id).subscribe(resp => {
      this.manufacturer = resp;
    });
  }

  save(f: NgForm) {
    if (f.invalid) {
      return;
    }
  
    if (this.manufacturer._id) {
      this._manufacturerService.update(this.manufacturer).subscribe(resp => {
        this.manufacturer = resp;
        this._router.navigate(['/admin/manufacturer', resp._id])
      })
    } else {
      this._manufacturerService.create(this.manufacturer).subscribe(resp => {
        this.manufacturer = resp;
        this._router.navigate(['/admin/manufacturer', resp._id])   
      });
    }
  }


}
