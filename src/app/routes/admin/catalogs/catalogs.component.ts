import { Component, OnInit } from '@angular/core';
import { Catalog } from '../../../beans/admin/catalog/catalog.bean';
import { Router } from '@angular/router';
import { CatalogService } from '../../../service/service.index';
const swal = require('sweetalert');

@Component({
  selector: 'app-catalogs',
  templateUrl: './catalogs.component.html',
  styles: []
})
export class CatalogsComponent implements OnInit {

  catalogs: Catalog[] = [];
  from: number = 0;
  total: number = 0;
  loading: boolean = true;

  constructor(
    private _catalogService: CatalogService,
    private _router: Router
  ) { }

  ngOnInit() {
    this.loadCatalogs();
  }


  loadCatalogs() {
    this.loading = true;
    this._catalogService.retrieveAll(this.from).subscribe((resp: any) => {
      this.total = resp.total;
      this.catalogs = resp.catalogs;
      this.loading = false;
    })
  }

  edit(catalog: Catalog) {
    this._router.navigate(['/admin/catalog', catalog._id])
  }

  delete(catalog: Catalog) {
    
    swal({
      title: '¿Esta seguro?',
      text: 'Esta a punto de borrar el cátalog' + catalog.name,
      icon: 'warning',
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        this._catalogService.delete(catalog._id).subscribe(resp => {
          this.from = 0;
          this.loadCatalogs();
        })
      }
    });

  }

  changePaginate(value: number) {
    let from  = this.from + value;

    if (from >= this.total) {
      return;
    }

    if (from < 0) {
      return;
    }
    this.from += value;
    this.loadCatalogs();
  }


}
