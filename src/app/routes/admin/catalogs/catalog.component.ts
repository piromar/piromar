import { Component, OnInit } from '@angular/core';
import { Catalog } from '../../../beans/admin/catalog/catalog.bean';
import { Router, ActivatedRoute } from '@angular/router';
import { CatalogService } from '../../../service/admin/catalog/catalog.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styles: []
})
export class CatalogComponent implements OnInit {

  catalog: Catalog = new Catalog('', '', '', '', '');

  constructor(
    private _catalogService: CatalogService,
    private _router: Router,
    public _activatedRoute: ActivatedRoute
  ) { 

    _activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id !== 'new') 
        this.load(id);
    })

  }

  ngOnInit() {
  }

  load(id: string) {
    this._catalogService.retrieve(id).subscribe(resp => {
      this.catalog = resp;
    });
  }

  save(f: NgForm) {
    if (f.invalid) {
      return;
    }
    this.catalog.valueType = 'String';
    if (this.catalog._id) {
      this._catalogService.update(this.catalog).subscribe(resp => {
        this.catalog = resp;
        this._router.navigate(['/admin/catalog', resp._id])
      })
    } else {
      this._catalogService.create(this.catalog).subscribe(resp => {
        this.catalog = resp;
        this._router.navigate(['/admin/catalog', resp._id])   
      });
    }
  }

}
