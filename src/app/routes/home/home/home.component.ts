import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../../service/service.index';
import { Product } from '../../../beans/admin/product/product.bean';
import { Router } from '@angular/router';


@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    products: Product[] = [];
    total: number = 0;

    constructor(
        private _productService: ProductService,
        public _router: Router
    ) { }

    ngOnInit() {
       
    }

    load() {
       
    }

    edit(product: Product) {
        
    }
    
    delete(product: Product) {
        
    }

    changePage(value: number) {
        
    }
    

}
