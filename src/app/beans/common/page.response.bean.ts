export class PageResponse{
    constructor(
        public content: any[],
        public page: number,
        public size: number,
        public totalElements: number,
        public totalPages: number,
        public last: boolean
    ) {}
}