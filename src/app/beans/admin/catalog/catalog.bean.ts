
export class Catalog {
    constructor(
        public code: string,
        public name: string,
        public family: string,
        public value: string,
        public valueType: string,
        public description?: string,
        public _id?: string
    ) {}
}