import { User } from './user.bean';
export class Contract {
    constructor(
        public contract: string,
        public signing: string,
        public position?: string,
        public codPromotion?: string,
        public contractType?: string,
        public annotation?: string,
        public status?: boolean,
        public user?: string,
        public _id?: string,
    ) {}
}