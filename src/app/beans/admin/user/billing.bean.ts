import { User } from './user.bean';

export class Billing {
    constructor(
        public company: string,
        public cif: string,
        public address: string,
        public postCode?: string,
        public provinceCode?: string,
        public townCode?: string,
        public iva?: number,
        public recharge?: number,
        public billingType?: string,
        public bic?: string,
        public iban?: string,
        public note?: string,
        public status?: boolean,
        public user?: string,
        public _id?: string
    ) {}
}