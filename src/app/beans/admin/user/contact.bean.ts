export class Contact {
    constructor(
        public name: string,
        public email: string,
        public phone?: string,
        public mobile?: string, 
        public timeTable?: string,
        public defaultContact?: boolean,
        public user?: string,
        public status?: boolean,
        public _id?: string
    ) {}
}