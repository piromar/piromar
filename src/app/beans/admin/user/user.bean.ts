export class User {
    constructor(
        public email: string,
        public password: string,
        public name?: string,
        public img?: string,
        public role?: string,
        public _id?: string
    ) {}
}