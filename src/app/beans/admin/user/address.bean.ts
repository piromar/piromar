import { User } from './user.bean';

export class Address {
    constructor(
        public name: string,
        public address: string,
        public postCode?: string,
        public townCode?: string,
        public provinceCode?: string,
        public phone?: string,
        public mobile?: string,
        public email?: string,
        public timeTable?: string,
        public defaultSystem?: boolean,
        public status?: boolean,
        public user?: string,
        public _id?: string
    ) {}
}