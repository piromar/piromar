import { Manufacturer } from './manufacturer.bean';

export class Product {
    constructor(
        public sku: string,
        public name: string,
        public description: string,
        public manufacturer?: Manufacturer,
        public weightNec?: number,
        public weightGross?: number,
        public unitBox?: number,
        public codDivision?: string,
        public codDivisionBox?: string,
        public codFamilyProduct?: string,
        public codUn?: string,
        public image?: string,
        public status?: boolean,
        public user?: string,
        public _id?: number
    ) {}
}