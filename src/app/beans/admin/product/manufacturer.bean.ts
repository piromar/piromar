export class Manufacturer {
    constructor(
        public code: string,
        public name: string,
        public cif: string,
        public mobile: string,
        public email: string,
        public phone?: string,
        public status?: boolean,
        public user?: string,
        public _id?: number
    ) {}
}