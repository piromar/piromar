import { Address } from '../admin/user/address.bean';
import { User } from '../admin/user/user.bean';
import { WithdrawalProduct } from './withdrawal.product.bean';

export class Withdrawal{
    constructor(
        public collectData: Date,
        public flgTransport: boolean,
        public descriptionTransport: string,
        public address?: Address,
        public user?: User,
        public withdrawalProduts?: WithdrawalProduct [],
        public status?: boolean,
        public id?: number
    ) {}
}