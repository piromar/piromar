import { Withdrawal } from './withdrawal.bean';
import { Product } from '../admin/product/product.bean';

export class WithdrawalProduct {
    constructor(
        public unit: number,
        public box: number,
        public withdrawal?: Withdrawal,
        public product?: Product,
        public id?: number
    ) {}
}