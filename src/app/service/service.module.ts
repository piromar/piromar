import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { LoginService } from './auth/login.service';
import { ProductService } from './admin/product/product.service';
import { ManufacturerService } from './admin/product/manufacturer.service';
import { LoginGuard } from './auth/login.guard';
import { CatalogService } from './admin/catalog/catalog.service';
import { UserService } from './admin/user/user.service';
import { AddressService } from './admin/user/address.service';
import { BillingService } from './admin/user/billing.service';
import { ContactService } from './admin/user/contact.service';
import { ContractService } from './admin/user/contract.service';
import { WithdrawalService } from './withdrawal/withdrawal.service';

@NgModule({
  imports: [
    HttpClientModule
  ],
  providers: [
    LoginService,
    LoginGuard,
    ProductService,
    ManufacturerService,
    CatalogService,
    UserService,
    AddressService,
    BillingService,
    ContactService,
    ContractService,
    WithdrawalService
  ]
})
export class ServiceModule { }