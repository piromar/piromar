import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { URL_MPR_PIROMAR_REST_API } from 'src/app/config/config';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { User } from '../../beans/admin/user/user.bean';

const swal = require('sweetalert');

@Injectable()
export class LoginService {

    token: string;
    url: string;
    userLogin: User;

    constructor(
        public _http: HttpClient,
        public _router: Router,
    ) {
        this.loadStorage();
    }

    login(user: User, remember: boolean = false)  {
        let url = `${URL_MPR_PIROMAR_REST_API}/login`;
        if (!remember) {
            localStorage.removeItem('email');
        }
        localStorage.setItem('email', user.email);
        
        return this._http.post(url, user).pipe(map((resp: any) => {
            this.createStorage(resp.token, resp.user);
        }), catchError(error => {
            return throwError(error);
        }));
    }

    logout() {
        localStorage.removeItem('token');
        this.token = null;
        this.userLogin = null;
    }

    hasLogin() {
        return this.token && this.token.length > 5 ? true : false;
    }

    createStorage(token: string, user: User) {
        localStorage.setItem('token', token);
        this.userLogin = user;
        this.token = token;
    }

    loadStorage() {
        if (!localStorage.getItem('token')) {
            this.token = '';
        } 

        this.token = localStorage.getItem('token');
    }
}