import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { LoginService } from './login.service';

@Injectable()
export class LoginGuard implements CanActivate {

    constructor(
        private _loginService: LoginService,
        private _router: Router
    ) {}

    canActivate(): boolean {
        
        if (!this._loginService.hasLogin() ) {
         
            this._router.navigate(['/login']);
            return false;
        
        }
        return true;
    }
}