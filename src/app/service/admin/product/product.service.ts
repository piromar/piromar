import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL_MPR_PIROMAR_REST_API } from 'src/app/config/config';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { Product } from '../../../beans/admin/product/product.bean';
const swal = require('sweetalert');

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  url: string;

  constructor(
    private _http: HttpClient
  ) {
    this.url = `${URL_MPR_PIROMAR_REST_API}/product`;
  }

  retrieveAll(from: number = 0) {
    return this._http.get(`${this.url}?from=${from}`);
  }

  retrieve(id: string) {
    return this._http.get(`${this.url}/${id}`).pipe(map((resp: any) => { 
      return resp.product;
    }), catchError(err => {
      throw err;
    }))
  }

  create(product: Product) {
    return this._http.post(this.url, product).pipe(map((resp: any) => {
      swal('Producto creado', product.name, 'success');
      return resp.product;
    }), catchError(err => {
      swal('Error, creando producto', err.err.message, 'error');
      return throwError(err);
    }));    
  }

  update(product: Product) {
    return this._http.put(`${this.url}/${product._id}`, product).pipe(map((resp: any) => {
      swal('Producto actualizado', product.name, 'success');
      return resp.product;
    }), catchError(err => {
      swal('Error, actualizando producto', err.err.message, 'error');
      throw err;
    }));
  }

  updateStatus(id: string) {
    return this._http.put(`${this.url}/${id}`, id).pipe(map((resp: any) => {
      swal('Producto actualizado', 'success');
      return resp.product;
    }), catchError(err => {
      swal('Error, actualizando producto', err.err.message, 'error');
      throw err;
    }));
  }

  delete(id: number) {
    return this._http.delete(`${this.url}/${id}`).pipe(map((resp: any) => {
       swal('Producto borrado', 'El producto ha sido eliminado correctamente', 'success');
       return true; 
    }), catchError(err => {
      swal('Error, borrando producto', err.err.message, 'error');
      throw err;
    }));
  }
}
