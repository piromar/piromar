import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL_MPR_PIROMAR_REST_API } from 'src/app/config/config';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { Manufacturer } from '../../../beans/admin/product/manufacturer.bean';
const swal = require('sweetalert');

@Injectable({
  providedIn: 'root'
})
export class ManufacturerService {

  url: string;

  constructor(
    private _http: HttpClient
  ) {
    this.url = `${URL_MPR_PIROMAR_REST_API}/manufacturer`;
  }
  retrieveAll(from: number = 0) {
    return this._http.get(`${this.url}?from=${from}`);
  }

  retrieve(id: string) {
    return this._http.get(`${this.url}/${id}`).pipe(map((resp: any) => { 
      return resp.manufacturer;
    }), catchError(err => {
      throw err;
    }))
  }

  create(manufacturer: Manufacturer) {
    return this._http.post(this.url, manufacturer).pipe(map((resp: any) => {
      swal('Fabricante creado', manufacturer.email, 'success');
      return resp.user;
    }), catchError(err => {
      swal('Error, creando fabricante', err.err.message, 'error');
      return throwError(err);
    }));    
  }

  update(manufacturer: Manufacturer) {
    return this._http.put(`${this.url}/${manufacturer._id}`, manufacturer).pipe(map((resp: any) => {
      swal('Fabricante actualizado', manufacturer.name, 'success');
      return resp.manufacturer;
    }), catchError(err => {
      swal('Error, actualizando fabricante', err.err.message, 'error');
      throw err;
    }));
  }

  updateStatus(id: string) {
    return this._http.put(`${this.url}/${id}`, id).pipe(map((resp: any) => {
      swal('Fabricante actualizado', 'success');
      return resp.manufacturer;
    }), catchError(err => {
      swal('Error, actualizando fabricante', err.err.message, 'error');
      throw err;
    }));
  }

  delete(id: number) {
    return this._http.delete(`${this.url}/${id}`).pipe(map((resp: any) => {
       swal('Fabricante borrado', 'El fabricante ha sido eliminado correctamente', 'success');
       return true; 
    }), catchError(err => {
      swal('Error, fabricante usuario', err.err.message, 'error');
      throw err;
    }));
  }
}
