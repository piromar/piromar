import { Injectable } from '@angular/core';
import { Address } from 'src/app/beans/admin/user/address.bean';
import { HttpClient } from '@angular/common/http';
import { URL_MPR_PIROMAR_REST_API } from 'src/app/config/config';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
const swal = require('sweetalert');

@Injectable({
  providedIn: 'root'
})
export class AddressService {

  url: string;

  constructor(
    private _http: HttpClient
  ) {
    this.url = `${URL_MPR_PIROMAR_REST_API}/address`;
  }

  findAll(idUser: string) {
    return this._http.get(`${this.url}/${idUser}`);
  }

  create(address: Address) {
    return this._http.post(this.url, address).pipe(map((resp: any) => {
      swal('Dirección creada', '', 'success');
      return resp.address;
    }), catchError(err => {
      swal('Error, creando dirección', err.err.message, 'error');
      return throwError(err);
    }));  
  }

  update(address: Address) {
    return this._http.put(`${this.url}/${address._id}`, address).pipe(map((resp: any) => {
      swal('Dirección actulizada', '', 'success');
      return resp.user;
    }), catchError(err => {
      swal('Error, actualizando dirección', err.err.message, 'error');
      throw err;
    }));
  }

  updateStatus(id: string) {
    return this._http.put(`${this.url}/${id}`, id).pipe(map((resp: any) => {
      return resp.address;
    }), catchError(err => {
      swal('Error, actualizando dirección', err.err.message, 'error');
      throw err;
    }));
  }

  delete(id: string) {
    return this._http.delete(`${this.url}/${id}`).pipe(map((resp: any) => {
      return true; 
   }), catchError(err => {
     swal('Error, borrando dirección', err.err.message, 'error');
     throw err;
   }));
  }

}
