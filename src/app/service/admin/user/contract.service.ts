import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL_MPR_PIROMAR_REST_API } from 'src/app/config/config';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { Contract } from '../../../beans/admin/user/contract.bean';
const swal = require('sweetalert');

@Injectable({
  providedIn: 'root'
})
export class ContractService {

  url: string;

  constructor(
    private _http: HttpClient
  ) {
    this.url = `${URL_MPR_PIROMAR_REST_API}/contract`;
  }

  findAll(idUser: string) {
    return this._http.get(`${this.url}/${idUser}`);
  }

  create(contract: Contract ) {
    return this._http.post(this.url, contract).pipe(map((resp: any) => {
      swal('Contrato Creado', '', 'success');
      return resp.contract;
    }), catchError(err => {
      swal('Error, creando contrato', err.err.message, 'error');
      return throwError(err);
    }));  
  }

  update(contract: Contract) {
    return this._http.put(`${this.url}/${contract._id}`, contract).pipe(map((resp: any) => {
      swal('Contrato Actualizado', '',  'success');
      return resp.contract;
    }), catchError(err => {
      swal('Error, actualizando contrato', err.err.message, 'error');
      throw err;
    }));
  }

  updateStatus(id: string) {
    return this._http.put(`${this.url}/${id}`, id).pipe(map((resp: any) => {
      return resp.contract;
    }), catchError(err => {
      swal('Error, actualizando contrato', err.err.message, 'error');
      throw err;
    }));
  }

  delete(id: string) {
    return this._http.delete(`${this.url}/${id}`).pipe(map((resp: any) => {
      return true; 
   }), catchError(err => {
     swal('Error, borrando contrato', err.err.message, 'error');
     throw err;
   }));
  }
  
}
