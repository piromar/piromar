import { Injectable } from '@angular/core';
import { User } from '../../../beans/admin/user/user.bean';

import { URL_MPR_PIROMAR_REST_API } from 'src/app/config/config';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

const swal = require('sweetalert');

@Injectable({
  providedIn: 'root'
})
export class UserService {

  url: string;

  constructor(
    private _http: HttpClient
  ) { 
    this.url = `${URL_MPR_PIROMAR_REST_API}/user`;
  }

  retrieveAll(from: number = 0) {
    return this._http.get(`${this.url}?from=${from}`);
  }

  retrieve(id: string) {
    return this._http.get(`${this.url}/${id}`).pipe(map((resp: any) => { 
      console.log(resp);
      return resp.user;
    }), catchError(err => {
      throw err;
    }))
  }

  create(user: User) {
    return this._http.post(this.url, user).pipe(map((resp: any) => {
      swal('Usuario creado', user.email, 'success');
      return resp.user;
    }), catchError(err => {
      swal('Error, creando usuario', err.err.message, 'error');
      return throwError(err);
    }));    
  }

  update(user: User) {
    return this._http.put(`${this.url}/${user._id}`, user).pipe(map((resp: any) => {
      swal('Usuario actualizado', user.email, 'success');
      return resp.user;
    }), catchError(err => {
      swal('Error, actualizando usuario', err.err.message, 'error');
      throw err;
    }));
  }

  updateStatus(id: string) {
    return this._http.put(`${this.url}/${id}`, id).pipe(map((resp: any) => {
      swal('Usuario actualizado', 'success');
      return resp.user;
    }), catchError(err => {
      swal('Error, actualizando usuario', err.err.message, 'error');
      throw err;
    }));
  }

  delete(id: string) {
    return this._http.delete(`${this.url}/${id}`).pipe(map((resp: any) => {
       swal('Usuario borrado', 'El usuario ha sido eliminado correctamente', 'success');
       return true; 
    }), catchError(err => {
      swal('Error, borrando usuario', err.err.message, 'error');
      throw err;
    }));
  }


}
