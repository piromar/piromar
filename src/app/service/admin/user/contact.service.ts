import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL_MPR_PIROMAR_REST_API } from 'src/app/config/config';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { Contact } from '../../../beans/admin/user/contact.bean';
const swal = require('sweetalert');

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  url: string;

  constructor(
    private _http: HttpClient
  ) {
    this.url = `${URL_MPR_PIROMAR_REST_API}/contact`;
  }

  findAll(idUser: String) {
    return this._http.get(`${this.url}/${idUser}`);
  }

  create(contact: Contact ) {
    return this._http.post(this.url, contact).pipe(map((resp: any) => {
      swal('Contacto creado', '', 'success');
      return resp.contact;
    }), catchError(err => {
      swal('Error, creando contacto', err.err.message, 'error');
      return throwError(err);
    }));  
  }

  update(contact: Contact) {
    return this._http.put(`${this.url}/${contact._id}`, contact).pipe(map((resp: any) => {
      swal('Contacto actualizado', '',  'success');
      return resp.contact;
    }), catchError(err => {
      swal('Error, actualizando contacto', err.err.message, 'error');
      throw err;
    }));
  }

  updateStatus(id: string) {
    return this._http.put(`${this.url}/${id}`, id).pipe(map((resp: any) => {
      return resp.contact;
    }), catchError(err => {
      swal('Error, actualizando contacto', err.err.message, 'error');
      throw err;
    }));
  }

  delete(id: string) {
    return this._http.delete(`${this.url}/${id}`).pipe(map((resp: any) => {
      return true; 
   }), catchError(err => {
     swal('Error, borrando contacto', err.err.message, 'error');
     throw err;
   }));
  }

}
