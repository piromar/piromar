import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL_MPR_PIROMAR_REST_API } from 'src/app/config/config';
import { Billing } from '../../../beans/admin/user/billing.bean';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
const swal = require('sweetalert');


@Injectable({
  providedIn: 'root'
})
export class BillingService {

  url: string;

  constructor(
    private _http: HttpClient
  ) {
    this.url = `${URL_MPR_PIROMAR_REST_API}/billing`;
  }

  findAll(idUser: string) {
    return this._http.get(`${this.url}/${idUser}`);
  }

  create(billing: Billing ) {
    return this._http.post(this.url, billing).pipe(map((resp: any) => {
      swal('Facturación creada', '', 'success');
      return resp.billing;
    }), catchError(err => {
      swal('Error, creando datos de facturación', err.message, 'error');
      return throwError(err);
    }));  
  }

  update(billing: Billing) {
    return this._http.put(`${this.url}/${billing._id}`, billing).pipe(map((resp: any) => {
      swal('Facturación actualizada', '', 'success');
      return resp.billing;
    }), catchError(err => {
      swal('Error, actualizando datos de facturación', err.err.message, 'error');
      throw err;
    }));
  }

  updateStatus(id: string) {
    return this._http.put(`${this.url}/${id}`, id).pipe(map((resp: any) => {
      return resp.billing;
    }), catchError(err => {
      swal('Error, actualizando datos de facturación', err.err.message, 'error');
      throw err;
    }));
  }

  delete(id: string) {
    return this._http.delete(`${this.url}/${id}`).pipe(map((resp: any) => {
      return true; 
   }), catchError(err => {
     swal('Error, borrando datos de facturación', err.err.message, 'error');
     throw err;
   }));
  }

}
