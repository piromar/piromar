import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL_MPR_PIROMAR_REST_API } from 'src/app/config/config';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { Catalog } from '../../../beans/admin/catalog/catalog.bean';
const swal = require('sweetalert');

@Injectable({
  providedIn: 'root'
})
export class CatalogService {

  url: string;

  constructor(
    private _http: HttpClient
  ) {
    this.url = `${URL_MPR_PIROMAR_REST_API}/catalog`;
  }

  retrieveAll(from: number = 0) {
    return this._http.get(`${this.url}?from=${from}`);
  }

  retrieveAllFamily(family: string) {
    return this._http.get(`${this.url}/family/${family}`);
  }

  retrieve(id: string) {
    return this._http.get(`${this.url}/${id}`).pipe(map((resp: any) => { 
      return resp.catalog;
    }), catchError(err => {
      throw err;
    }))
  }

  create(catalog: Catalog) {
    return this._http.post(this.url, catalog).pipe(map((resp: any) => {
      swal('Catálogo creado', catalog.name, 'success');
      return resp.catalog;
    }), catchError(err => {
      swal('Error, creando catálogo', err.err.message, 'error');
      return throwError(err);
    }));    
  }

  update(catalog: Catalog) {
    return this._http.put(`${this.url}/${catalog._id}`, catalog).pipe(map((resp: any) => {
      swal('Catálogo actualizado', catalog.name, 'success');
      return resp.catalog;
    }), catchError(err => {
      swal('Error, actualizando catálogo', err.err.message, 'error');
      throw err;
    }));
  }

  updateStatus(id: string) {
    return this._http.put(`${this.url}/${id}`, id).pipe(map((resp: any) => {
      swal('Catálogo actualizado', 'success');
      return resp.catalog;
    }), catchError(err => {
      swal('Error, actualizando catálogo', err.err.message, 'error');
      throw err;
    }));
  }

  delete(id: string) {
    return this._http.delete(`${this.url}/${id}`).pipe(map((resp: any) => {
       swal('Catálogo borrado', 'El catálogo ha sido eliminado correctamente', 'success');
       return true; 
    }), catchError(err => {
      swal('Error, borrando catálogo', err.err.message, 'error');
      throw err;
    }));
  }

}
